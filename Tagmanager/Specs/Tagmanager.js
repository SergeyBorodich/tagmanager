"use strict";

var LoginPage = require('../PageObjects/Login.js');
var Account_config = require('../PageObjects/Account_config.js');
var Container_config = require('../PageObjects/Container_config.js');
var CreateCancel = require('../PageObjects/CreateBtn_disabled.js');

describe ('Проверка элементов страницы для создания нового аккаунта', function() {

    beforeAll(function(){
        browser.ignoreSynchronization = true;
        browser.get('https://tagmanager.google.com/#/admin/accounts/create/');
        browser.driver.sleep(1000);
        LoginPage.login();
        browser.ignoreSynchronization = false;
        browser.waitForAngular();

    });

    afterEach(function(){
        CreateCancel.check();
    });

    it('Проверить наличие поля "Название аккаунта"', function() {
        browser.driver.sleep(3000);
        Account_config.a_check();
    });

    it('Ввод значения в поле "Название аккаунта" и проверить его', function() {
         Account_config.fill();
         browser.driver.sleep(1000);
         Account_config.checkAccText();
    });

    it('Выбрать чек-бокс "Передавать анонимные данные в Google и другие службы" и проверить его', function() {
        Account_config.activate();
        browser.driver.sleep(1000);
        Account_config.checkBox();
    });

    it('Нажать кнопку "Далее" и проверить наличие поля "Название контейнера"', function(){
        Account_config.nextClick();
        browser.driver.sleep(1000);
        browser.waitForAngular();
        Container_config.checkCont();
    });

    it('Ввод значения в поле "Название контейнера" и проверить его', function(){
        Container_config.fillCont();
        browser.driver.sleep(1000);
        Container_config.checkContText();
    });

    it('Нажать кнопку "Отмена" и проверить, что поле "Название аккаунта" пустое и поле "Название контейнера" отсутствует', function() {
        CreateCancel.cancelBtnClick();
        browser.driver.sleep(2000);
        Account_config.checkAccTextEmpty();
        Container_config.checkContDis();
    });

});