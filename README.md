# Tagmanager #

Автоматизированное тестирование страницы добавления нового аккаунта на сайте [tagmanager.google.com](https://tagmanager.google.com/#/admin/accounts/create)

## Для запуска теста  ##

1. Установить [JDK](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) и [Node.js](https://nodejs.org/en/).
2. Установить Protractor с помощью команды в командной строке: 
npm install -g protractor
3. Обновить Selenium server с помощью команды: webdriver-manager update
4. Запустить сервер с помощью команды: webdriver-manager start
5. Чтобы запустить выполнение теста необходимо в командной строке перейти в каталог проекта: cd 'путь к папке с файлом conf.js'. И далее выполнить команду запуска:
protractor conf.js