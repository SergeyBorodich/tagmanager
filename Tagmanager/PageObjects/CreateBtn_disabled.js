var CreateCancel = function(){

    var CreateBtn = element(by.buttonText('Создать')),
        CancelBtn = element(by.buttonText('Отмена'));

    this.check = function(){
        expect(CreateBtn.isEnabled()).toBe(false);
    };

    this.cancelBtnClick = function() {
        CancelBtn.click();
    }
};
module.exports = new CreateCancel();