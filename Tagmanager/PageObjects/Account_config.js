var Account_config = function() {

    var accName = element(by.id('2-form.account.data.name')),
        box = element(by.id('2-form.account.data.shareData')),
        next = element(by.buttonText('Далее'));

    this.a_check = function() {
        browser.isElementPresent(by.id('2-form.account.data.name'));
    };

    this.fill = function() {
        accName.sendKeys('MyCompany');
    };

    this.checkAccText = function() {
        expect(accName.getAttribute('value')).toEqual('MyCompany');
    };

    this.checkAccTextEmpty = function() {
        expect(accName.getAttribute('value')).toEqual('');
    };

    this.activate = function() {
        box.click();
    };

    this.checkBox = function() {
        expect(box.getAttribute('class')).toContain('material-checked');
    };

    this.nextClick = function(){
        next.click();
    };
};
module.exports = new Account_config();