exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',
    capabilities: {'browserName': 'chrome'},
    specs: ['Specs/Tagmanager.js'],
    jasmineNodeOpts: {
        showColors: true,
        defaultTimeoutInterval: 30000
    }
};