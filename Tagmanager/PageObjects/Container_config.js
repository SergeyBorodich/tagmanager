var Container_config = function() {

    var contName = element(by.id('1-form.container.data.name'));

    this.checkCont = function() {
        expect(contName.isPresent()).toBe(true);
    };

    this.fillCont = function() {
        contName.sendKeys('MySite');
    };

    this.checkContText = function() {
        expect(contName.getAttribute('value')).toEqual('MySite');
    };

    this.checkContDis = function() {
        expect(contName.isPresent()).toBe(false);
    };
};
module.exports = new Container_config();